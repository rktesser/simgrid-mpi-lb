/* Copyright (c) 2013-2017. The SimGrid Team. All rights reserved.          */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */

#ifndef SMPI_COLLS_PRIVATE_H
#define SMPI_COLLS_PRIVATE_H

#include <math.h>
#include "smpi/mpi.h"
#include "private.h"
#include "smpi_coll.hpp"
#include "smpi_comm.hpp"
#include "smpi_datatype.hpp"
#include "smpi_op.hpp"
#include "smpi_request.hpp"

XBT_LOG_EXTERNAL_DEFAULT_CATEGORY(smpi_colls);

#endif
