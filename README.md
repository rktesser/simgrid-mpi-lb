# This is an __unnofficial fork__ of SimGrid!

**The official simgrid can be found [here](http://simgrid.gforge.inria.fr/).**

This fork of SimGrid is intended to support the **[SAMPI dynamic load balancing simulator](https://bitbucket.org/rktesser/smpi-lb)**.

---

SimGrid is a scientific instrument to study the behavior of 
large-scale distributed systems such as Grids, Clouds, HPC or P2P
systems. It can be used to evaluate heuristics, prototype applications 
or even assess legacy MPI applications.

More documentation is included in this archive (doc/html/index.html)
or [online](http://simgrid.gforge.inria.fr/)

In any case, you may want to subscribe to the [user mailing list](http://lists.gforge.inria.fr/mailman/listinfo/simgrid-user). 
There, you can find answers to your questions, or simply discuss with
people doing the same kind of research than you do, in an active and
friendly community.

Thanks for using our software. Please do great things with it and tell
the world about it. Tell us, too, because we love to have positive
feedback.

Cheers,
Da SimGrid Team.

[license-badge]: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
[release-badge]: https://img.shields.io/github/release/simgrid/simgrid.svg
[release-link]:  https://gforge.inria.fr/frs/?group_id=12
